console.log("Hello World")


// 3. Create a variable number that will store the value of the number provided by the user via the prompt.

// let count = Number(prompt("Give a number:"))

// 4. Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.

// 5. Create a condition that if the current value is less than or equal to 50, stop the loop.

    for (let count = 100; count > 0 ; count--) {
        if(count % 10 === 0) {
            console.log("The number is divisible by 10. Skipping the number ")
            continue;
        }

        if (count % 5 === 0) {
            console.log(count);
            continue;
        }

        if(count < 55) {
            console.log("The current value is at 50. Terminating the loop");
            break;
        }
    }

// 6. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.


// 7. Create another condition that if the current value is divisible by 5, print the number.


// 8. Create a variable that will contain the string supercalifragilisticexpialidocious.

let word = "sUpErcAlifragilisticexpialidocious";
console.log(word);
let answer = "";

// Using function then for loop
function removeVowel(word) {

    let vowels = [ 'a', 'e', 'i', 'o', 'u' ];
    let result = "";
    
    for(let i = 0; i < word.length; i++)
    {
    
        if (!vowels.includes(word[i].toLowerCase()))
        {
            result += word[i];
        }
    }
    return result;
}

console.log(removeVowel(word));


// Using for loop only
for (let i = 0; i < word.length; i++) {

    if (word[i].toLowerCase() == 'a' ||
    word[i].toLowerCase() == 'e' || 
    word[i].toLowerCase() == 'i' || 
    word[i].toLowerCase() == 'o' || 
    word[i].toLowerCase() == 'u') {

        continue;
    }
        answer += word[i];
}
console.log(answer);


// s -> s


// 9. Create another variable that will store the consonants from the string.


// 10.Create another for Loop that will iterate through the individual letters of the string based on it’s length.


// 11. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.


// 12. Create an else statement that will add the letter to the second variable.